
using UnityEngine;
using System;
using System.Collections;

public class PickUpController : MonoBehaviour
{
    [Header("Pick-up Settings")]
    [SerializeField] private float _dropForwardForce;

    [SerializeField] private float _dropUpwardForce;

    [SerializeField] private float _pickupRadius;

    [Header("Rigging Config")]
    [SerializeField] private Transform _weaponAttachPoint;

    [Tooltip("This object will hold the mag, for use when reloading.")]
    [SerializeField] private Transform _magHolder;

    [Tooltip("The parent when the mag is on the gun")]
    [SerializeField] private Transform _magSourceParentGun;

    [Tooltip("Where the left hand should grab the mag")]
    [SerializeField] private Transform _magHoldIK;

    [Tooltip("Where the left hand should grab the gun")]
    [SerializeField] private Transform _gripHoldIK;

    public event Action<Weapon> OnPickupItem = delegate{};
    public event Action<Weapon> OnDropItem = delegate{};

    public Weapon CurrentWeapon{ get; private set; }

    public bool HasWeaponEquipped() => CurrentWeapon != null;

    private void Start()
    {
        // Search for weapon if already equipped.
        Weapon weapon = _weaponAttachPoint.GetComponentInChildren<Weapon>();

        if (weapon) Pickup(weapon);
    }

    private IEnumerator SetupRigConstraintsDelayed(Weapon weapon)
    {
        yield return null;

        // Move the mag to the mag holder. This is necessary since the
        // reload animation will be grabbing this object.
        weapon.Mag.parent = _magHolder;
        weapon.Mag.localPosition = Vector3.zero;
        weapon.Mag.localRotation = Quaternion.identity;

        // Copy transform of the mag
        _magSourceParentGun.position = weapon.MagSourceParent.position;
        _magSourceParentGun.rotation = weapon.MagSourceParent.rotation;

        // Set the target when grabbing the mag
        _magHoldIK.position = weapon.MagTarget.position;
        _magHoldIK.rotation = weapon.MagTarget.rotation;

        // Set the target when grabbing the grip
        _gripHoldIK.position = weapon.GripTarget.position;
        _gripHoldIK.rotation = weapon.GripTarget.rotation;
    }

    public void OnPickupEvent()
    {
        Vector3 origin = Camera.main.transform.position;
        Vector3 dir = Camera.main.transform.forward;

        RaycastHit[] hits = Physics.SphereCastAll(origin, 3f, dir, _pickupRadius);

        foreach (RaycastHit hit in hits)
        {
            if (hit.transform.root.TryGetComponent<Weapon>(out Weapon weapon))
            {
                Debug.Log($"Picking up weapon: '{weapon}'");
                Pickup(weapon);
                return;
            }
        }
    }

    public void Pickup(Weapon weapon)
    {
        if (HasWeaponEquipped())
            OnDropEvent();

        // Make weapon a child of the holder
        weapon.transform.SetParent(_weaponAttachPoint);
        weapon.transform.localPosition = weapon.MountPositionOffset;
        weapon.transform.localRotation = Quaternion.Euler(weapon.MountRotationOffset);
        weapon.transform.localScale = Vector3.one;

        // Setup the IK rig for this item
        // NOTE: We need to wait a frame for the gun to be parented to the holder first.
        StartCoroutine(SetupRigConstraintsDelayed(weapon));

        if (weapon.gameObject.TryGetComponent<Rigidbody>(out Rigidbody rb))
        {
            // Make Rigidbody kinematic and BoxCollider a trigger
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        CurrentWeapon = weapon;
        OnPickupItem(CurrentWeapon);
    }

    public void OnDropEvent()
    {
        if (!HasWeaponEquipped())
            return;

        // Reset the rig back to normal
        CurrentWeapon.Mag.SetParent(CurrentWeapon.transform, true);

        // Set parent to null
        CurrentWeapon.transform.SetParent(null);

        if (CurrentWeapon.gameObject.TryGetComponent<Rigidbody>(out Rigidbody rb))
        {
            // Make Rigidbody not kinematic
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.mass = 50;

            // Keep momentum of the player?
            // rb.velocity = player.GetComponent<Rigidbody>().velocity;
            
            // AddForce
            rb.AddForce(Camera.main.transform.forward * _dropForwardForce, ForceMode.Impulse);
            rb.AddForce(Camera.main.transform.up * _dropUpwardForce, ForceMode.Impulse);
            
            // Add random rotation
            float random = UnityEngine.Random.Range(-1f, 1f);
            rb.AddTorque(new Vector3(random, random, random));
        }

        OnDropItem(CurrentWeapon);
        CurrentWeapon = null;
    }
}
