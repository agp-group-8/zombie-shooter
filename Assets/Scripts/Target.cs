
using UnityEngine;

public interface Target
{
    public void Process(RaycastHit hit);
}
