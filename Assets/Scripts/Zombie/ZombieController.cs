using System.Collections;

using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ZombieHealth))]
public class ZombieController : MonoBehaviour
{
    public float lookRadius = 8f;
    public float zombieAttackInterval = 1.0f;
    public float zombieDamage = 10.0f;

    private Transform _target;
    private NavMeshAgent _agent;
    private AudioSource _groan;
    private Animator _animator;

    private int _animID_IsAttackingID;
    private int _animID_RunStateID;
    private int _animID_IsDeadID;
    private int _animID_TriggerScreamID;

    private int _animID_SpeedMultiplierID;

    private float _timeToScream;
    private bool _alreadyScreamed;

    private ZombieHealth _health;

    private void Awake()
    {
        _target = PlayerManager.Instance.player.transform;
        _agent = GetComponent<NavMeshAgent>();
        _groan = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();
        _health = GetComponent<ZombieHealth>();

        _animID_IsAttackingID = Animator.StringToHash("IsAttacking");
        _animID_IsDeadID = Animator.StringToHash("IsDead");
        _animID_RunStateID = Animator.StringToHash("Running");
        _animID_TriggerScreamID = Animator.StringToHash("TriggerScream");

        _animID_SpeedMultiplierID = Animator.StringToHash("SpeedMultiplier");
    }

    private void Start()
    {
        StartCoroutine(damageInterval(zombieAttackInterval));

        _timeToScream = Time.time + Random.Range(4f, 10f);

        _health.OnDead += OnDead;
    }

    private void OnDestroy()
    {
        _health.OnDead -= OnDead;
    }

    void Update()
    {
        float distance = Vector3.Distance(_target.position, transform.position);

        if (!_alreadyScreamed && Time.time >= _timeToScream)
        {
            _animator.SetTrigger(_animID_TriggerScreamID);
            _alreadyScreamed = true;
            return;
        }

        // We wait for the scream to finish.
        // May not be the best solution but it works.
        //
        if (_animator.GetCurrentAnimatorStateInfo(0).shortNameHash == _animID_RunStateID && distance <= lookRadius)
        {
            _agent.SetDestination(_target.position);
            _animator.SetFloat(_animID_SpeedMultiplierID, 1f / _agent.speed);
            // PlaySound();
        }
        else
        {
            _agent.SetDestination(transform.position);
            _groan.Stop();
        }

        // this is what to do if the zombie reaches you - zombie attack
        _animator.SetBool(_animID_IsAttackingID, distance <= _agent.stoppingDistance);
        FaceTarget();
    }

    private void OnDead()
    {
        // zombie is dead, remove from scene and inc player score
        HUD.instance.UpdatePlayerScore(10);

        _animator.SetBool(_animID_IsDeadID, true);
        _agent.SetDestination(transform.position);
        
        // Stop damage coroutines
        StopAllCoroutines();

        // Destroy the object after some delay
        Destroy(gameObject, 6f);

        // Don't update script anymore
        enabled = false;
    }

    private void PlaySound()
    {
        if (!_groan.isPlaying)
        {
            _groan.Play();
        }
    }

    private void FaceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private IEnumerator damageInterval(float interval)
    {
        while (true)
        {
            float distance = Vector3.Distance(_target.position, transform.position);

            if (distance <= _agent.stoppingDistance)
            {
                FirstPersonController.OnTakeDamage(zombieDamage);
                yield return new WaitForSeconds(interval);
            }

            yield return null;
        }
    }
}
