
using UnityEngine;
using System;

public class ZombieHealth : MonoBehaviour, Target
{
    [field: SerializeField] public int MaxHealth{ get; private set; }

    public int CurrentHealth{ get; private set; }

    public static event Action<ZombieHealth> OnHealthAdded = delegate{};
    public static event Action<ZombieHealth> OnHealthRemoved = delegate{};

    public event Action<float> OnHealthPctChange = delegate{};
    public event Action OnDead = delegate{};

    private void Start()
    {
        CurrentHealth = MaxHealth;
    }

    public void Process(RaycastHit hit)
    {
        // We were hit for the first time, show the health bar.
        if (CurrentHealth == MaxHealth)
            OnHealthAdded(this);

        CurrentHealth -= 15;

        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            OnDead();

            OnHealthRemoved(this);
        }

        float changePct = (float) CurrentHealth / MaxHealth;
        OnHealthPctChange(changePct);
    }
}
