using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject zombiePrefab;

    [SerializeField]
    int zombiesToSpawn;

    //[SerializeField]
    //GameObject spawnLocation;
    Vector3 spawnLocation;

    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;

    Quaternion zombieOrientation = Quaternion.Euler(90, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
         spawnLocation = gameObject.transform.position;

        //for (int i = 0; i < zombiesToSpawn; i++){
        //    SpawnZombie(spawnLocation.transform.position);
        //}

        InvokeRepeating("SpawnZombie", spawnTime, spawnDelay);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    GameObject SpawnZombie()
    {
        if (stopSpawning)
        {
            // stop spawning zombies
        }
        return Instantiate(zombiePrefab, spawnLocation, zombieOrientation);
    }


}
