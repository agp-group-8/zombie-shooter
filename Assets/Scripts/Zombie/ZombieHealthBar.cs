
using UnityEngine;
using UnityEngine.UI;

public class ZombieHealthBar : MonoBehaviour
{
    [SerializeField] private Image _foreground;

    [SerializeField] private float _offset;

    private ZombieHealth _health;

    public void SetHealth(ZombieHealth health)
    {
        _health = health;
        _health.OnHealthPctChange += OnHealthChange;
    }

    private void OnDestroy()
    {
        _health.OnHealthPctChange -= OnHealthChange;
    }

    private void OnHealthChange(float pct)
    {
        float updateTime = (_foreground.fillAmount - pct) * 0.2f;

        LeanTween.value(_foreground.gameObject, _foreground.fillAmount, pct, updateTime)
            .setOnUpdate((float value) => _foreground.fillAmount = value);
    }

    private void LateUpdate()
    {
        transform.position = _health.transform.position + Vector3.up * _offset;

        transform.LookAt(Camera.main.transform);
        transform.Rotate(0, 180, 0);
    }
}
