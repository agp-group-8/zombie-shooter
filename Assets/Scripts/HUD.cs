using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
//public class Singleton: MonoBehaviour
{
    public static HUD instance;

    void Awake()
    {
        instance = this;
    }

    public GameObject waveSpawner;
    public Slider healthBar;
    public int playerScore = 0;
    public GameObject playerGameObject;
    public Sprite AK47Image;
    public Sprite AK47BulletImage;

    private PickUpController pickupControllerScript;

    private WaveSpawner waveSpawnerScript;

    private GameObject waveGameObject;
    private TMPro.TextMeshProUGUI waveText;

    private GameObject scoreGameObject;
    private TMPro.TextMeshProUGUI scoreText;

    private GameObject ammoGameObject;
    private TMPro.TextMeshProUGUI ammoText;

    private Image gunImage;
    private Image bulletImage;

    // Start is called before the first frame update
    void Start()
    {
        waveSpawnerScript = waveSpawner.GetComponent<WaveSpawner>();
        waveGameObject = gameObject.transform.GetChild(2).gameObject;
        waveText = waveGameObject.GetComponent<TMPro.TextMeshProUGUI>();

        scoreGameObject = gameObject.transform.GetChild(1).gameObject;
        scoreText = scoreGameObject.GetComponent<TMPro.TextMeshProUGUI>();
        scoreText.text = "Score: 0";

        ammoGameObject = gameObject.transform.GetChild(4).gameObject;
        ammoText = ammoGameObject.GetComponent<TMPro.TextMeshProUGUI>();

        gunImage = ammoGameObject.transform.GetChild(0).gameObject.GetComponent<Image>();
        bulletImage = ammoGameObject.transform.GetChild(1).gameObject.GetComponent<Image>();

        pickupControllerScript = playerGameObject.GetComponent<PickUpController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Current wave:" + waveSpawnerScript.nextWave);
        UpdateWaveNumber();

        UpdateAmmo();
        PlayerPrefs.SetInt("Player Score", playerScore);
    }

    private void UpdateWaveNumber()
    {
        string waveTextPrefix = "Wave: ";
        waveText.text = waveTextPrefix + (waveSpawnerScript.nextWave + 1); //waves are 0-indexed

    }

    public void UpdatePlayerScore(int toAdd)
    {
        string scoreTextPrefix = "Score:";
        scoreText.text = scoreTextPrefix + (playerScore += toAdd);
    }

    public void UpdateHealth(float value)
    {
       healthBar.value = value;
    }

    public void UpdateAmmo()
    {
        if (!pickupControllerScript.HasWeaponEquipped())
        {
            ammoText.text = "";

            // change the alpha of the images when gun not equiped
            gunImage.sprite = null;
            bulletImage.sprite = null;
            
            var tmpGunColor = gunImage.color;
            var tmpBulletColor = bulletImage.color;
            tmpGunColor.a = 0f;
            tmpBulletColor.a = 0f;
            gunImage.color = tmpGunColor;
            bulletImage.color = tmpBulletColor;
        }
        else
        {
            // change the alpha of the images when gun IS equiped
            var tmpColor = gunImage.color;
            var tmpBulletColor = bulletImage.color;
            tmpColor.a = 1f;
            tmpBulletColor.a = 1f;
            gunImage.color = tmpColor;
            bulletImage.color = tmpBulletColor;
            gunImage.sprite = AK47Image;
            bulletImage.sprite = AK47BulletImage;
            int maxAmmo = pickupControllerScript.CurrentWeapon.MaxAmmo;
            int currentAmmo = pickupControllerScript.CurrentWeapon.CurrentAmmo;
            ammoText.text = currentAmmo + " / " + maxAmmo;
        }


    }
}
