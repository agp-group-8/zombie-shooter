
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [field: SerializeField] public Transform GripTarget{ get; private set; }

    [field: SerializeField] public Transform MagTarget{ get; private set; }

    [field: SerializeField] public Transform MagSourceParent{ get; private set; }

    [field: SerializeField] public Transform Mag{ get; private set; }
}
