using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject hudScore;
    //private GameOverScreen gameOverScript;

    private FirstPersonController fpsScript;

    private void Start()
    {
        fpsScript = Player.GetComponent<FirstPersonController>();
        //gameOverScript = gameObject.GetComponent<GameOverScreen>();
    }

    private void Update()
    {
        if (fpsScript._currentHealth <= 0)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        print(hudScore.GetComponent<TMPro.TextMeshProUGUI>().text);
        string scoreText = hudScore.GetComponent<TMPro.TextMeshProUGUI>().text.ToString();
        //GameOverScreen.instance.Setup(scoreText);
        SceneManager.LoadScene("GameOverScene");
        Debug.Log("GameManager: GAME OVER!");
        
    }

}
