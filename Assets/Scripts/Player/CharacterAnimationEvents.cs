
using System;
using UnityEngine;

public class CharacterAnimationEvents : MonoBehaviour
{
    public delegate void OnReloadStartEvent();
    public OnReloadStartEvent onReloadStartEvent;

    public delegate void OnReloadFinishEvent();
    public OnReloadFinishEvent onReloadFinishEvent;

    public delegate void OnMagRemoveEvent();
    public OnMagRemoveEvent onMagRemoveEvent;

    public delegate void OnMagAttachEvent();
    public OnMagAttachEvent onMagAttachEvent;

    public event Action OnRackWeaponEvent = delegate { };

    private void OnReloadStart()
    {
        onReloadStartEvent?.Invoke();
    }

    private void OnReloadFinish()
    {
        onReloadFinishEvent?.Invoke();
    }

    private void OnMagRemove()
    {
        onMagRemoveEvent?.Invoke();
    }

    private void OnMagAttach()
    {
        onMagAttachEvent?.Invoke();
    }

    private void OnRackWeapon()
    {
        OnRackWeaponEvent();
    }
}
