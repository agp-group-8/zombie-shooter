﻿using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using System;
using System.Collections;

[RequireComponent(typeof(CharacterController), typeof(PlayerInput))]
public class FirstPersonController : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private float _moveSpeed = 4.0f;
    
    [SerializeField] private float _sprintSpeed = 6.0f;
    
    [Tooltip("Rotation speed of the character")]
    [SerializeField] private float _rotationSpeed = 1.0f;
    
    [Tooltip("Acceleration and deceleration")]
    [SerializeField] private float _speedChangeRate = 10.0f;
    
    [Tooltip("Animation blendtree change speed")]
    [SerializeField] private float _animBlendSpeed = 12.0f;

    [SerializeField] private Animator _animator;

    [SerializeField] private Transform _fpsHands;

    [Space(10)]
    [SerializeField] private float _jumpHeight = 1.2f;
    
    [SerializeField] private float _gravity = -15.0f;

    [Space(10)]
    [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    [SerializeField] private float _jumpTimeout = 0.1f;
    
    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    [SerializeField] private float _fallTimeout = 0.15f;

    [Header("Camera")]
    [Tooltip("Camera rotation object that controls ")]
    [SerializeField] private Transform _cameraRot;

    [SerializeField] private CinemachineVirtualCamera _adsCamera;

    [Tooltip("How far in degrees can you move the camera up")]
    [SerializeField] private float _topClamp = 90.0f;
    
    [Tooltip("How far in degrees can you move the camera down")]
    [SerializeField] private float _bottomClamp = -90.0f;

    [Header("Sound Effects")]
    [SerializeField] private float _baseStepSpeed;
    
    [SerializeField] private float _sprintStepMultiplier;
    
    [SerializeField] private AudioClip[] _sfxFootSteps;

    [SerializeField] private AudioClip _sfxJump;

    [SerializeField] private AudioClip _sfxLand;

    private float _footStepTimer = 0;
    private float CurrentFootStepOffset => _input.sprint ? _baseStepSpeed * _sprintStepMultiplier : _baseStepSpeed;

    private Weapon _weapon;
    private RuntimeAnimatorController _defaultAnimationController;

    // camera
    private float _cameraTargetPitch;
    private float _cameraTargetYaw;

    // player
    private float _speed;
    private float _verticalVelocity;
    private float _terminalVelocity = 53.0f;
    private Vector3 _lastMoveDir = Vector3.zero;

    [SerializeField] private float _maxHealth = 100f;
    [SerializeField] public float _currentHealth;
    public delegate void OnPlayerDeath();
    public event OnPlayerDeath playerDied;


    public static Action<float> OnTakeDamage;
    public static Action<float> OnDamage;
    public static Action<float> WaveEndHeal; // only used at end of waves
    public GameObject gunContainer;

    // timeout deltatime
    private float _jumpTimeoutDelta;
    private float _fallTimeoutDelta;

    private CharacterController _controller;
    private FirstPersonInputs _input;
    private PickUpController _pickUpController;
    private AudioSource _audioSource;
    private CharacterAnimationEvents _animationEvents;

    // Animation
    private int _animID_DirectionX;
    private int _animID_DirectionY;
    private int _animID_ReloadTrigger;
    private int _animID_ThrowTrigger;
    private int _animID_IsFiring;
    private int _animID_IsJumping;
    private int _animID_IsADS;

    private int _animID_ReloadMultiplier;
    private int _animID_ShootMultiplier;

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
        _input = GetComponent<FirstPersonInputs>();
        _pickUpController = GetComponent<PickUpController>();
        _audioSource = GetComponent<AudioSource>();
        _animationEvents = GetComponentInChildren<CharacterAnimationEvents>();

        _pickUpController.OnPickupItem += OnPickupItem;
        _pickUpController.OnDropItem += OnDropItem;

        _animationEvents.OnRackWeaponEvent += OnRackWeaponEvent;
        _animationEvents.onMagAttachEvent += OnMagClipEvent;
        _animationEvents.onMagRemoveEvent += OnMagClipEvent;

        _animID_DirectionX = Animator.StringToHash("DirectionX");
        _animID_DirectionY = Animator.StringToHash("DirectionY");
        _animID_ReloadTrigger = Animator.StringToHash("ReloadTrigger");
        _animID_ThrowTrigger = Animator.StringToHash("ThrowTrigger");
        _animID_IsJumping = Animator.StringToHash("IsJumping");
        _animID_IsFiring = Animator.StringToHash("IsFiring");
        _animID_IsADS = Animator.StringToHash("IsADS");

        _animID_ReloadMultiplier = Animator.StringToHash("ReloadMultiplier");
        _animID_ShootMultiplier = Animator.StringToHash("ShootMultiplier");

        _defaultAnimationController = _animator.runtimeAnimatorController;
    }

    private void Start()
    {
        _currentHealth = _maxHealth;
        _jumpTimeoutDelta = _jumpTimeout;
        _fallTimeoutDelta = _fallTimeout;
    }

    private void Update()
    {
        JumpAndGravity();
        Move();
        Footsteps();

        if (_weapon != null)
            Shooting();
    }

    private void LateUpdate()
    {
        CameraRotation();
    }

    private void OnEnable()
    {
        OnTakeDamage += ApplyDamage;
        WaveEndHeal += healDamage;
    }

    private void OnDisable()
    {
        OnTakeDamage -= ApplyDamage;
        WaveEndHeal -= healDamage;
    }

    private void OnPickupItem(Weapon weapon)
    {
        _animator.runtimeAnimatorController = weapon.AnimatorController;

        _weapon = weapon;
        _weapon.OnReloadWeapon += OnWeaponReload;
        _weapon.OnMount();
        _animator.SetFloat(_animID_ReloadMultiplier, 1.0f / _weapon.ReloadTime);
    }

    private void OnDropItem(Weapon _)
    {
        _animator.runtimeAnimatorController = _defaultAnimationController;

        _animator.SetTrigger(_animID_ThrowTrigger);
        _animator.SetBool(_animID_IsADS, false);
        _animator.SetBool(_animID_IsFiring, false);

        _weapon.OnReloadWeapon -= OnWeaponReload;
        _weapon = null;
    }

    private void Shooting()
    {
        if (_input.fire && _weapon.CanShoot())
            _weapon.OnAttack();

        // Animations
        bool playADSanimation = _input.ads && !_weapon.IsReloading;
        bool playShootAnimation = _input.fire && !_weapon.IsReloading;

        _adsCamera.enabled = playADSanimation;

        _animator.SetBool(_animID_IsADS, playADSanimation);
        _animator.SetBool(_animID_IsFiring, playShootAnimation);
    }

    public void OnReloadEvent()
    {
        if (_weapon != null)
            _weapon.OnReload();
    }

    private void OnMagClipEvent()
    {
        _audioSource.PlayOneShot(_weapon.SoundEffects.sfxClipMagazine);
    }

    private void OnRackWeaponEvent()
    {
        _audioSource.PlayOneShot(_weapon.SoundEffects.sfxRack);
    }

    private void OnWeaponReload()
    {
        _animator.SetTrigger(_animID_ReloadTrigger);
    }

    private void CameraRotation()
    {
        // Don't multiply mouse input by Time.deltaTime
        _cameraTargetPitch += _input.look.y * _rotationSpeed;
        _cameraTargetYaw += _input.look.x * _rotationSpeed;

        // Clamp our pitch rotation
        _cameraTargetPitch = ClampAngle(_cameraTargetPitch, _bottomClamp, _topClamp);

        // Update camera target pitch and yaw
        _cameraRot.localRotation = Quaternion.Euler(_cameraTargetPitch, _cameraTargetYaw, 0.0f);
    }

    private void Move()
    {
        // set target speed based on move speed, sprint speed and if sprint is pressed
        float targetSpeed = _input.sprint ? _sprintSpeed : _moveSpeed;

        // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

        // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is no input, set the target speed to 0
        if (_input.move == Vector2.zero) targetSpeed = 0.0f;

        // a reference to the players current horizontal velocity
        float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

        float speedOffset = 0.1f;
        float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;

        // accelerate or decelerate to target speed
        if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
        {
            // creates curved result rather than a linear one giving a more organic speed change
            // note T in Lerp is clamped, so we don't need to clamp our speed
            _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * _speedChangeRate);

            // round speed to 3 decimal places
            _speed = Mathf.Round(_speed * 1000f) / 1000f;
        }
        else
        {
            _speed = targetSpeed;
        }

        // normalise input direction
        Vector3 right = _cameraRot.right; right.y = 0.0f;
        Vector3 forward = _cameraRot.forward; forward.y = 0.0f;

        Vector3 inputDirection = _lastMoveDir;

        if (_controller.isGrounded)
            inputDirection = right * _input.move.x + forward * _input.move.y;

        Vector3 velocity = inputDirection.normalized * _speed + new Vector3(0.0f, _verticalVelocity, 0.0f);

        // move the player
        _controller.Move(velocity * Time.deltaTime);

        float animationBlendX = _animator.GetFloat(_animID_DirectionX);
        animationBlendX = Mathf.Lerp(animationBlendX, _input.move.x * targetSpeed / _sprintSpeed, Time.deltaTime * _animBlendSpeed);
        _animator.SetFloat(_animID_DirectionX, animationBlendX);

        float animationBlendZ = _animator.GetFloat(_animID_DirectionY);
        animationBlendZ = Mathf.Lerp(animationBlendZ, _input.move.y, Time.deltaTime * _animBlendSpeed);
        _animator.SetFloat(_animID_DirectionY, animationBlendZ);

        _lastMoveDir = inputDirection;
    }

    private void Footsteps()
    {
        if (!_controller.isGrounded)
            return;

        if (_input.move == Vector2.zero)
            return;

        _footStepTimer -= Time.deltaTime;

        if (_footStepTimer <= 0)
        {
            _audioSource.PlayOneShot(_sfxFootSteps[UnityEngine.Random.Range(0, _sfxFootSteps.Length)]);
            _footStepTimer = CurrentFootStepOffset;
        }
    }

    private void JumpAndGravity()
    {
        if (_controller.isGrounded)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = _fallTimeout;

            // stop our velocity dropping infinitely when grounded
            if (_verticalVelocity < 0.0f)
            {
                _verticalVelocity = -2f;
            }

            // Jump
            if (_input.jump && _jumpTimeoutDelta <= 0.0f)
            {
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                _verticalVelocity = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
                _audioSource.PlayOneShot(_sfxJump);
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
            {
                _jumpTimeoutDelta -= Time.deltaTime;
            }
        }
        else
        {

            // reset the jump timeout timer
            _jumpTimeoutDelta = _jumpTimeout;

            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }

            // if we are not grounded, do not jump
            _input.jump = false;
        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (_verticalVelocity < _terminalVelocity)
        {
            _verticalVelocity += _gravity * Time.deltaTime;
        }

        _animator.SetBool(_animID_IsJumping, !_controller.isGrounded);
    }

    private void ApplyDamage(float dmg)
    {
        _currentHealth -= dmg;
        OnDamage?.Invoke(_currentHealth);
        HUD.instance.UpdateHealth(_currentHealth);
        if (_currentHealth <= 0)
        {
            // game over

            playerDeath();
        }
    }

    private void healDamage(float healAmt)
    {
        if (_currentHealth < 70 && _currentHealth >= 0)
            _currentHealth += healAmt;
        else
            _currentHealth = _maxHealth;
        HUD.instance.UpdateHealth(_currentHealth);
    }

    private void playerDeath()
    {
        _currentHealth = 0;
        //Debug.Log("Game over");
        // TODO: Death sound, animation
    }

    public float getHealth()
    {
        return _currentHealth;
    }

    public float getMaxHealth()
    {
        return _maxHealth;
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
}
