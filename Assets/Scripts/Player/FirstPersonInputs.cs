using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class FirstPersonInputs : MonoBehaviour
{
    [Header("Character Input Values")]
    [HideInInspector] public Vector2 move;
    [HideInInspector] public Vector2 look;
    [HideInInspector] public bool jump;
    [HideInInspector] public bool sprint;
    [HideInInspector] public bool fire;
    [HideInInspector] public bool ads;

    [SerializeField] private UnityEvent OnReloadEvent;
    [SerializeField] private UnityEvent OnPickupEvent;
    [SerializeField] private UnityEvent OnDropEvent;

    [Header("Movement Settings")]
    [HideInInspector] public bool analogMovement;

    [Header("Mouse Cursor Settings")]
    [HideInInspector] public bool cursorLocked = true;
    [HideInInspector] public bool cursorInputForLook = true;

    public void OnMove(InputValue value)
    {
        MoveInput(value.Get<Vector2>());
    }

    public void OnLook(InputValue value)
    {
        if (cursorInputForLook)
        {
            LookInput(value.Get<Vector2>());
        }
    }

    public void OnJump(InputValue value)
    {
        JumpInput(value.isPressed);
    }

    public void OnSprint(InputValue value)
    {
        SprintInput(value.isPressed);
    }

    public void OnFire(InputValue value)
    {
        FireInput(value.isPressed);
    }

    public void OnAimDownSights(InputValue value)
    {
        AimDownSightsInput(value.isPressed);
    }

    public void OnReload(InputValue value)
    {
        if (value.isPressed)
            OnReloadEvent?.Invoke();
    }

    public void OnPickup(InputValue value)
    {
        if (value.isPressed)
            OnPickupEvent?.Invoke();
    }

    public void OnDrop(InputValue value)
    {
        if (value.isPressed)
            OnDropEvent?.Invoke();
    }


    public void MoveInput(Vector2 newMoveDirection)
    {
        move = newMoveDirection;
    }

    public void LookInput(Vector2 newLookDirection)
    {
        look = newLookDirection;
    }

    public void JumpInput(bool newJumpState)
    {
        jump = newJumpState;
    }

    public void SprintInput(bool newSprintState)
    {
        sprint = newSprintState;
    }

    public void FireInput(bool newFireState)
    {
        fire = newFireState;
    }

    public void AimDownSightsInput(bool newAdsState)
    {
        ads = newAdsState;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        SetCursorState(cursorLocked);
    }

    private void Start()
    {
        SetCursorState(true);
    }

    private void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }
}
