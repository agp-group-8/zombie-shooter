using UnityEngine;

[CreateAssetMenu(fileName = "NewImpactEffectsTemplate", menuName = "Effects/Impact Effects Template")]
public class WeaponImpactEffects : ScriptableObject
{
    public ParticleSystem pfImpactMetal;

    public ParticleSystem pfImpactWood;

    public ParticleSystem pfImpactSand;

    public ParticleSystem pfImpactStone;

    public ParticleSystem pfImpactFlesh;
}
