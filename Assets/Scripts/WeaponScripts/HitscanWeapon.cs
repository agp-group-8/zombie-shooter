
using System.Collections;
using UnityEngine;

public class HitscanWeapon : Weapon
{
    [Header("Weapon Specs")]
    [SerializeField] protected float _range = 100f;

	[SerializeField] protected float _bulletSpeed;

    [SerializeField] protected LayerMask _collisionLayers;

	[Header("Effects Config")]
    [SerializeField] protected ParticleSystem _muzzleFlash;

    [SerializeField] protected ParticleSystem _muzzleSmoke;

    [SerializeField] protected Transform _firePoint;

    [SerializeField] protected GameObject _pfBullet;

    [SerializeField] protected WeaponImpactEffects _hitEffectsTemplate;

    protected AudioSource _audioSource;

	public override void OnMount()
	{
        CurrentAmmo = MaxAmmo;

        _recoil = GetComponentInParent<Recoil>();
        _audioSource = GetComponentInParent<AudioSource>();

        if (_recoil)
        {
            _recoil.recoilX = _recoilX;
            _recoil.recoilY = _recoilY;
            _recoil.recoilZ = _recoilZ;
            _recoil.snappiness = _recoilSnappiness;
            _recoil.returnSpeed = _recoilReturnSpeed;
        }
	}

	public override void OnAttack()
	{
        if (!CanShoot())
            return;
            
        base.OnAttack();

		if (_muzzleFlash != null)
            _muzzleFlash.Emit(1);

		if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, _range, _collisionLayers))
		{
			// NOTE: We need to get transform.root now since the hitboxes may be child gameobjects of the target!
			if (hit.transform.root.TryGetComponent<Target>(out Target target))
				target.Process(hit);

            SpawnHitParticle(hit.point, hit.normal, hit.transform);
		}
		else
		{
			hit.point = Camera.main.transform.position + Camera.main.transform.forward * 1000f;
			hit.normal = transform.forward;
		}

		if (_recoil)
			_recoil.RecoilFire();

        _audioSource.PlayOneShot(SoundEffects.sfxAttack);
		SpawnBullet(hit.point);

		if (NeedReload())
            OnReload();
	}

	public override void OnReload()
	{
        if (!CanReload())
            return;

		IsReloading = true;

        StartCoroutine(ReloadCoroutine());

        base.OnReload();
	}

	private void SpawnBullet(Vector3 point)
	{
        GameObject bullet = Instantiate(_pfBullet, _firePoint.position, Quaternion.LookRotation(_firePoint.up));

        StartCoroutine(BulletTrailCoroutine(bullet, point));

        _muzzleSmoke.Emit(1);
	}

    private void SpawnHitParticle(Vector3 pos, Vector3 normal, Transform target)
    {
		ParticleSystem pfHitEffect = _hitEffectsTemplate.pfImpactSand;

		switch (target.tag)
		{
			case "Stone": pfHitEffect = _hitEffectsTemplate.pfImpactStone; break;
			case "Metal": pfHitEffect = _hitEffectsTemplate.pfImpactMetal; break;
			case "Sand": pfHitEffect = _hitEffectsTemplate.pfImpactSand; break;
			case "Wood": pfHitEffect = _hitEffectsTemplate.pfImpactWood; break;
			case "Zombie": pfHitEffect = _hitEffectsTemplate.pfImpactFlesh; break;
		}

        ParticleSystem hitEffect = Instantiate(pfHitEffect, pos, Quaternion.LookRotation(normal), target);

		hitEffect.Emit(1);

        Destroy(hitEffect.gameObject, hitEffect.main.duration);
    }

    // Coroutines
    private IEnumerator BulletTrailCoroutine(GameObject trail, Vector3 point)
    {
        Vector3 start = trail.transform.position;
        Vector3 dir = _firePoint.forward;
        float max_distance = Vector3.Distance(start, point);

        while (Vector3.Distance(trail.transform.position, start) < max_distance)
        {
            trail.transform.position += dir * _bulletSpeed * Time.deltaTime;
            yield return null;
        }

        Destroy(trail.gameObject);
    }

	private IEnumerator ReloadCoroutine()
	{
		yield return new WaitForSeconds(ReloadTime);

		CurrentAmmo = MaxAmmo;

        IsReloading = false;
	}

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
		Gizmos.color = Color.green;
        Gizmos.DrawLine(_firePoint.position, _firePoint.position + _firePoint.forward * 100f);
    }
#endif
}
