using UnityEngine;

[CreateAssetMenu(fileName = "NewSoundEffectsTemplate", menuName = "Effects/Sound Effects Template")]
public class WeaponSoundEffects : ScriptableObject
{
    public AudioClip sfxAttack;

    public AudioClip sfxClipMagazine;

    public AudioClip sfxRack;
}
