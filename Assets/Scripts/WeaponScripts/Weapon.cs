
using System;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public event Action OnReloadWeapon = delegate {};

    [field: Header("Specs")]
    [Tooltip("How long to wait before completely reloading the mag (seconds)")]
    [field: SerializeField] public float ReloadTime { get; protected set; }

    [Tooltip("Fire rate of the weapon in Rounds Per Minute")]
    [field: SerializeField] public float FireRate { get; protected set; }

    [field: SerializeField] public int MaxAmmo { get; protected set; }

    [field: SerializeField] public int BaseDamage { get; protected set; }

    [field: SerializeField] public RuntimeAnimatorController AnimatorController { get; protected set; }

    [field: SerializeField] public WeaponSoundEffects SoundEffects { get; protected set; }

    [Header("Recoil")]
    [SerializeField] protected float _recoilX;

    [SerializeField] protected float _recoilY;

    [SerializeField] protected float _recoilZ;

    [SerializeField] protected float _recoilSnappiness;

    [SerializeField] protected float _recoilReturnSpeed;

    [field: Header("Animation Setup")]
    // Where the free hand should grip the weapon
    [field: SerializeField] public Transform GripTarget { get; protected set; }

    // Where the free gun should grip the magazine
    [field: SerializeField] public Transform MagTarget { get; protected set; }

    // Transform holding a copy of the magazine's default position
    [field: SerializeField] public Transform MagSourceParent { get; protected set; }

    // Transform of the magazine
    [field: SerializeField] public Transform Mag { get; protected set; }
    
    // Positional offset of weapon when mounted.
    [field: SerializeField] public Vector3 MountPositionOffset { get; protected set; }

    // Rotational offset of weapon when mounted.
    [field: SerializeField] public Vector3 MountRotationOffset { get; protected set; }

    public bool IsReloading { get; protected set; }

    public int CurrentAmmo { get; protected set; }

    protected Recoil _recoil;

    protected float _fireInterval;

    protected float _nextTimeToFire;

    protected float _lastFired;

    private void Start() => _fireInterval = 60 / FireRate;

    // Returns a bool indicating if the weapon requires a reload
    // usually means the weapon has 0 ammo.
    public virtual bool NeedReload() => CurrentAmmo == 0 && !IsReloading;

    // Returns a bool indicating if the weapon is able to be reloaded
    // usually means the weapon has less than max ammo.
    public virtual bool CanReload() => CurrentAmmo < MaxAmmo && !IsReloading;

    // Returns a bool indicating if the weapon is able to shoot.
    public virtual bool CanShoot() => Time.time >= _nextTimeToFire && CurrentAmmo > 0 && !IsReloading;

    // Called when the weapon should attack.
    public virtual void OnAttack()
    {
        --CurrentAmmo;
        _lastFired = Time.time;
        _nextTimeToFire = Time.time + _fireInterval;
    }

    // Called when the weapon is mounted on the player.
    public virtual void OnMount() {}

    // Called when the weapon should reload.
    public virtual void OnReload() => OnReloadWeapon();
}
