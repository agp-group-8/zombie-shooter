
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : MonoBehaviour
{
    [field: SerializeField] public Slider LoadingBar{ get; private set; }
    
    private void Start()
    {
        LoadingBar.value = 0f;
    }    
}
