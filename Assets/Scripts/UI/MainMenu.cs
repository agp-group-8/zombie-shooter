
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private LoadingPanel _loadingPanel;

    [SerializeField] private GameObject _mainMenu;

    [SerializeField] private string _gameSceneName;

    public void OnPlayBtnClick()
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(_gameSceneName, LoadSceneMode.Single);

        _loadingPanel.gameObject.SetActive(true);
        _mainMenu.SetActive(false);

        StartCoroutine(UpdateLoadBarCoroutine(op));
    }

    private IEnumerator UpdateLoadBarCoroutine(AsyncOperation op)
    {
        while (true)
        {
            _loadingPanel.LoadingBar.value = op.progress;

            yield return null;
        }
    }
}