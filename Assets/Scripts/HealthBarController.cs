
using UnityEngine;
using System.Collections.Generic;

public class HealthBarController : MonoBehaviour
{
    [SerializeField] private ZombieHealthBar _pfHealthBar;

    private Dictionary<ZombieHealth, ZombieHealthBar> _healthBars = new Dictionary<ZombieHealth, ZombieHealthBar>();

    private void Awake()
    {
        ZombieHealth.OnHealthAdded += AddHealthBar;
        ZombieHealth.OnHealthRemoved += RemoveHealthBar;
    }

    private void OnDestroy()
    {
        ZombieHealth.OnHealthAdded -= AddHealthBar;
        ZombieHealth.OnHealthRemoved -= RemoveHealthBar;
    }

    private void AddHealthBar(ZombieHealth health)
    {
        if (!_healthBars.ContainsKey(health))
        {
            ZombieHealthBar healthBar = Instantiate(_pfHealthBar, transform);
            _healthBars.Add(health, healthBar);
            healthBar.SetHealth(health);
        }
    }

    private void RemoveHealthBar(ZombieHealth health)
    {
        if (_healthBars.ContainsKey(health))
        {
            Destroy(_healthBars[health].gameObject);
            _healthBars.Remove(health);

        }
    }
}
