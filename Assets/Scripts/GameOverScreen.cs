using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public static GameOverScreen instance;

    void Awake()
    {
        instance = this;
    }

    public GameObject points;
    private TMPro.TextMeshProUGUI pointsText;

    private void Start()
    {
        pointsText = points.GetComponent<TMPro.TextMeshProUGUI>();
        SetCursorState(false);
    }

    private void Update()
    {
        pointsText.text = "Score: " + PlayerPrefs.GetInt("Player Score").ToString();
    }

    private void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("MainScene");
        Debug.Log("GAME RESET");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MenuScene");
        Debug.Log("Game over - going back to main menu");
    }

}
