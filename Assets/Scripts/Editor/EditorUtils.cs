
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Linq;

public class EditorUtils
{
    static EditorUtils()
    {
        Menu.SetChecked("Util/Developer Mode", EditorPrefs.GetBool("DeveloperMode"));
    }

    [MenuItem("Util/Screenshot")]
    static void Screenshot()
    {
        // NOTE: Taken from https://stackoverflow.com/a/71082189

        string folderPath = "Screenshots/";

        if (!System.IO.Directory.Exists(folderPath))
            System.IO.Directory.CreateDirectory(folderPath);

        string screenshotName = $"Screenshot_{System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss")}.png";
        string filePath = System.IO.Path.Combine(folderPath, screenshotName);
        
        ScreenCapture.CaptureScreenshot(filePath, 1);
        Debug.Log($"Saved '{filePath}'");
    }

    ///
    /// Select one or multiple GameObjects in the inspector
    ///
    [MenuItem("Util/Remove missing script from selection")]
    static void RemoveMissingScript()
    {
        int count = 0;
        foreach (GameObject obj in Selection.gameObjects)
        {
            Debug.Log($"Checking {obj.name} for missing scripts...");
            count += RemoveMissingScriptRecursive(obj);
        }
        
        if (count > 0)
        {
            Debug.Log($"Removed {count} missing scripts.");
            return;
        }

        Debug.Log("No missing scripts found.");
    }

    private static int RemoveMissingScriptRecursive(GameObject obj)
    {
        int count = GameObjectUtility.RemoveMonoBehavioursWithMissingScript(obj);

        for (int i = 0; i < obj.transform.childCount; ++i)
            count += RemoveMissingScriptRecursive(obj.transform.GetChild(i).gameObject);

        return count;
    }

    ///
    /// Recursively remove all missing scripts in the scene
    ///
    [MenuItem("Util/Remove all missing scripts in scene %&r")] //Keyboard shortcut: CTRL(%) + ALT(&) + R
    static void RemoveMissingScripts()
    {
        var RootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        
        for (int rootObjects = 0; rootObjects < RootGameObjects.Length; rootObjects++)
        {
            //Find all children of a root GameObject
            var children = RootGameObjects[rootObjects].GetComponentsInChildren<Transform>();
            for (int i = 0; i < children.Length; i++)
            {
                GameObjectUtility.RemoveMonoBehavioursWithMissingScript(children[i].gameObject);
            }
        }

        var disabledGameObjects = GetAllDisabledSceneObjects();
        for (int i = 0 ; i < disabledGameObjects.Length; i++)
        {
            GameObjectUtility.RemoveMonoBehavioursWithMissingScript(disabledGameObjects[i].gameObject);
        }
    }

    [MenuItem("Util/Developer Mode")]
    static void SetDeveloperMode()
    {
        EditorPrefs.SetBool("DeveloperMode", !EditorPrefs.GetBool("DeveloperMode"));
        Menu.SetChecked("Util/Developer Mode", EditorPrefs.GetBool("DeveloperMode"));
    }

    static Transform[] GetAllDisabledSceneObjects()
    {
        var allTransforms = Resources.FindObjectsOfTypeAll(typeof(Transform));
        var previousSelection = Selection.objects;
        Selection.objects = allTransforms.Cast<Transform>()
            .Where(x => x != null )
            .Select(x => x.gameObject)
            .Where(x => x != null && !x.activeInHierarchy)
            .Cast<Object>().ToArray();
       
        var selectedTransforms = Selection.GetTransforms( SelectionMode.Editable | SelectionMode.ExcludePrefab);
        Selection.objects = previousSelection;
     
        return selectedTransforms;
    }
}
