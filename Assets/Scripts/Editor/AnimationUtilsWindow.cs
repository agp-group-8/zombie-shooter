
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AnimationUtilsWindow : EditorWindow
{
    private AnimationClip _lastAnimationClip;

    private Dictionary<EditorCurveBinding, float[]> _savedDeltas = new Dictionary<EditorCurveBinding, float[]>();

    private void SaveAnimationDeltas()
    {
        AnimationWindow window = EditorWindow.GetWindow<AnimationWindow>();

        if (window == null)
        {
            Debug.LogError("Animation Window must be open for this!");
            return;
        }

        if (window.animationClip == null)
        {
            Debug.LogError("Animation Clip must be selected for this!");
            return;
        }

        _lastAnimationClip = window.animationClip;

        Debug.Log($"{window.animationClip.name} at {window.time} (Frame: {window.frame})");

        AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(window.animationClip);

        EditorCurveBinding[] bindings = AnimationUtility.GetCurveBindings(window.animationClip);

        _savedDeltas.Clear();

        foreach (EditorCurveBinding binding in bindings)
        {
            AnimationCurve curve = AnimationUtility.GetEditorCurve(window.animationClip, binding);

            float lastValue = curve.keys[0].value;

            float[] curveDeltas = new float[curve.keys.Length - 1];
            _savedDeltas.Add(binding, curveDeltas);

            // Calculate deltas
            for (int i = 1; i < curve.keys.Length; ++i)
            {
                curveDeltas[i - 1] = curve.keys[i].value - lastValue;
                lastValue = curve.keys[i].value;
            }
        }
    }

    private void ApplyAnimationDeltas()
    {
        AnimationWindow window = EditorWindow.GetWindow<AnimationWindow>();

        if (window == null)
        {
            Debug.LogError("Animation Window must be open for this!");
            return;
        }

        if (window.animationClip == null)
        {
            Debug.LogError("Animation Clip must be selected for this!");
            return;
        }

        if (_lastAnimationClip != window.animationClip)
        {
            Debug.LogError("Applying deltas to incorrect animation clip! Did you mean to save deltas first?");
            return;
        }

        AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(window.animationClip);

        EditorCurveBinding[] bindings = AnimationUtility.GetCurveBindings(window.animationClip);

        foreach (EditorCurveBinding binding in bindings)
        {
            AnimationCurve curve = AnimationUtility.GetEditorCurve(window.animationClip, binding);

            float lastValue = curve.keys[0].value;

            if (!_savedDeltas.TryGetValue(binding, out float[] curveDeltas))
                continue;

            // Apply Deltas
            lastValue = curve.keys[0].value;

            for (int i = 1; i < curve.keys.Length; ++i)
            {
                float newValue = curveDeltas[i - 1] + lastValue;
                ReplaceKey(curve, i, newValue);
                lastValue = newValue;
            }

            AnimationUtility.SetEditorCurve(window.animationClip, binding, curve);
        }

        _savedDeltas.Clear();
        _lastAnimationClip = null;
    }

    private void OnGUI()
    {
        GUILayout.BeginVertical();

        if (_lastAnimationClip != null)
        {
            GUILayout.Label($"Saved deltas for {_lastAnimationClip.name}");

            if (GUILayout.Button("Apply Deltas..."))
                ApplyAnimationDeltas();

            if (GUILayout.Button("Clear"))
            {
                _savedDeltas.Clear();
                _lastAnimationClip = null;
            }
        }
        else
        {
            if (GUILayout.Button("Save Deltas..."))
                SaveAnimationDeltas();
        }

        GUILayout.EndVertical();
    }

    private static void ReplaceKey(AnimationCurve curve, int index, float value)
    {
        Debug.Assert(index < curve.keys.Length);

        Keyframe key = curve.keys[index];
        curve.RemoveKey(index);
        curve.AddKey(key.time, value);
    }

    [MenuItem("Window/Animation Utils/Animation Deltas")]
    public static void ShowWindow()
    {
        var window = GetWindow<AnimationUtilsWindow>(typeof(AnimationUtilsWindow));
        window.minSize = new Vector2(50, 250);
        window.Show();
    }
}
