using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    public class Wave
    {
        public string name;
        public GameObject zombiePrefab;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    public int nextWave = 0;

    public float timeBetweenWaves = 5f;
    public float waveCountDown;

    private float searchCountDown = 1f;

    private SpawnState state = SpawnState.COUNTING;

    public FirstPersonController player; // To grab the health

    private void Start()
    {
        waveCountDown = timeBetweenWaves;

    }

    private void Update()
    {
        if (state == SpawnState.WAITING)
        {
            // check if player killed all the zombies in a wave
            if (ZombiesStillAlive() == false)
            {
                // wave complete start a new round
                WaveCompleted();
            }
            else
            {
                // do this to skip the rest of the code in update() - don't need to if zombies still alive
                return;
            }
        }

        if (waveCountDown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                // start a wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        Debug.Log("Wave Completed!");
        FirstPersonController.WaveEndHeal(player.getHealth());

        state = SpawnState.COUNTING;
        waveCountDown = timeBetweenWaves;

        // temp out of bounds check
        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("Completed ALL waves, reseting wave number to 0 ...");
        }
        else
        {
            nextWave++;
        }


    }

    bool ZombiesStillAlive()
    {
        searchCountDown -= Time.deltaTime;
        if (searchCountDown <= 0)
        {
            searchCountDown = 1f;
            // only search once a second for zombies
            if (GameObject.FindGameObjectWithTag("Zombie") == null)
            {
                // player killed all zombies
                return false;
            }
        }

        // zombies still alive
        return true;
    }

    IEnumerator SpawnWave(Wave wave)
    {
        Debug.Log("Spawning Wave: " + wave.name);
        state = SpawnState.SPAWNING;

        for (int i = 0; i < wave.count; i++)
        {
            SpawnZombie(wave.zombiePrefab);
            // this is basically the delay between spawning enemies
            yield return new WaitForSeconds(1f / wave.rate); 
        }

        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnZombie(GameObject zombie)
    {
        // spawn enemy
        Debug.Log("Spawning Enemy: " + zombie.name);
        Instantiate(zombie, transform.position, transform.rotation);
    }
}
